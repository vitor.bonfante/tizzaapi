﻿namespace TizzaApi
{
    public class InserirPromoverDTO
    {
        public string Descricao { get; set; }
        public decimal Valor { get; set; }
        public DateTime DataVigencia { get; set; }
        public int IdPizzaria { get; set; }
    }
}
