﻿namespace TizzaApi
{
    public interface IServPromover
    {
        public void Inserir(InserirPromoverDTO inserirDto);
    }
    public class ServPromover : IServPromover
    {
        private DataContext _dataContext;

        public ServPromover(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Inserir(InserirPromoverDTO inserirDto)
        {
            var promover = new Promover();

            promover.Descricao = inserirDto.Descricao;
            promover.Valor = inserirDto.Valor;
            promover.DataVigencia = inserirDto.DataVigencia;
            promover.IdPizzaria = inserirDto.IdPizzaria;

            promover.Status = EnumStatusPromover.EmAberto;

            _dataContext.Add(promover);
            _dataContext.SaveChanges();
        }
    }

    

}
