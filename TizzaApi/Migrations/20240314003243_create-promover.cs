﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TizzaApi.Migrations
{
    /// <inheritdoc />
    public partial class createpromover : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Pizzaria",
                table: "Pizzaria");

            migrationBuilder.RenameTable(
                name: "Pizzaria",
                newName: "pizzarias");

            migrationBuilder.AddPrimaryKey(
                name: "PK_pizzarias",
                table: "pizzarias",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "promover",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Descricao = table.Column<string>(type: "TEXT", nullable: false),
                    Valor = table.Column<decimal>(type: "TEXT", nullable: false),
                    DataVigencia = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Status = table.Column<int>(type: "INTEGER", nullable: false),
                    IdPizzaria = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_promover", x => x.Id);
                    table.ForeignKey(
                        name: "FK_promover_pizzarias_IdPizzaria",
                        column: x => x.IdPizzaria,
                        principalTable: "pizzarias",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_promover_IdPizzaria",
                table: "promover",
                column: "IdPizzaria");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "promover");

            migrationBuilder.DropPrimaryKey(
                name: "PK_pizzarias",
                table: "pizzarias");

            migrationBuilder.RenameTable(
                name: "pizzarias",
                newName: "Pizzaria");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Pizzaria",
                table: "Pizzaria",
                column: "Id");
        }
    }
}
